import 'package:flutter/material.dart';

class ComponentMiniCard extends StatelessWidget {
  const ComponentMiniCard({super.key, required this.imageName, required this.imageText});

  final String imageName;
  final String imageText;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          SizedBox(
            width: 100,
            height: 100,
            child: Image.asset('assets/${imageName}'),
          ),
          Container(
            child: Text(imageText),
          )
        ],
      ),
    );
  }
}
