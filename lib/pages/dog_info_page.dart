import 'package:flutter/material.dart';

class DogInfoPage extends StatelessWidget {
  const DogInfoPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('귀여운 강아지 소개'),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            const SizedBox(
              height: 20,
            ),
            SizedBox(
              width: MediaQuery.of(context).size.width,
              height: 200,
              child: Image.asset('assets/dog.jpg'),
            ),
            const SizedBox(
              height: 15,
            ),
            const Center(
              child: Text('귀여운 우리 강아지'),
            ),
            const SizedBox(
              height: 20,
            ),
            SizedBox(
              width: MediaQuery.of(context).size.width,
              height: 200,
              child: Image.asset('assets/dog.jpg'),
            ),
            const SizedBox(
              height: 15,
            ),
            const Center(
              child: Text('귀여운 우리 강아지'),
            ),
            const SizedBox(
              height: 20,
            ),
            SizedBox(
              width: MediaQuery.of(context).size.width,
              height: 200,
              child: Image.asset('assets/dog.jpg'),
            ),
            const SizedBox(
              height: 15,
            ),
            const Center(
              child: Text('귀여운 우리 강아지'),
            ),
          ],
        ),
      )
    );
  }
}
