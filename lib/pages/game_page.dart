import 'package:flutter/material.dart';

class GamePage extends StatefulWidget {
  const GamePage({Key? key}) : super(key: key);

  @override
  State<GamePage> createState() => _GamePageState();
}

class _GamePageState extends State<GamePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          SizedBox(
            width: MediaQuery.of(context).size.width,
            height: 300,
            child: Image.asset(
              'assets/stone_bg.jpeg',
              fit: BoxFit.fill,
            ),
          ),
          Positioned(
            top: 50,
            left: MediaQuery.of(context).size.width / 2 - 65,
            child: SizedBox(
              width: 130,
              height: 130,
              child: Image.asset('assets/gok.png'),
            ),
          ),
          Positioned(
            top: 180,
            left: MediaQuery.of(context).size.width / 2 - 25,
            child: ElevatedButton(
              onPressed: () {},
              child: Text("캐기"),
            ),
          ),
        ],
      ),
    );
  }
}
