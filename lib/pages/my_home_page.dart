import 'package:flutter/material.dart';

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Column(
        mainAxisSize: MainAxisSize.max,
        children: [
          Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Container(
                color: Colors.deepOrange,
                width: MediaQuery.of(context).size.width / 2,
                height: 150,
                child: Center(
                  child: Text('1'),
                ),
              ),
              Container(
                color: Colors.amber,
                width: MediaQuery.of(context).size.width / 2,
                height: 150,
                child: Center(
                  child: Text('2'),
                ),
              )
            ],
          ),
          Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Container(
                color: Colors.amberAccent,
                width: MediaQuery.of(context).size.width / 2,
                height: 150,
                child: Center(
                  child: Text('3'),
                ),
              ),
              Container(
                color: Colors.green,
                width: MediaQuery.of(context).size.width / 2,
                height: 150,
                child: Center(
                  child: Text('4'),
                ),
              )
            ],
          )
        ],
      ),
    );
  }
}