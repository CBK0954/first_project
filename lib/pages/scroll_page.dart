import 'package:flutter/material.dart';

class ScrollPage extends StatelessWidget {
  const ScrollPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                children: [
                  Container(
                    color: Colors.green,
                    width: 300,
                    height: 150,
                  ),
                  Container(
                    color: Colors.amberAccent,
                    width: 300,
                    height: 150,
                  ),
                  Container(
                    color: Colors.green,
                    width: 300,
                    height: 150,
                  ),
                  Container(
                    color: Colors.amberAccent,
                    width: 300,
                    height: 150,
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Container(
              color: Colors.deepOrange,
              height: 1300,
            ),
          ],
        ),
      ),
    );
  }
}
