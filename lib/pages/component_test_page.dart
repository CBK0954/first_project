import 'package:first_project/components/component_mini_card.dart';
import 'package:flutter/material.dart';

class ComponentTestPage extends StatefulWidget {
  const ComponentTestPage({Key? key}) : super(key: key);

  @override
  State<ComponentTestPage> createState() => _ComponentTestPageState();
}

class _ComponentTestPageState extends State<ComponentTestPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Row(
            children: [
              ComponentMiniCard(imageName: 'dog.jpg', imageText: '귀여우니1'),
              ComponentMiniCard(imageName: 'gok.png', imageText: '귀여우니2'),
              ComponentMiniCard(imageName: 'stone_bg.jpeg', imageText: '귀여우니3'),
            ],
          )
        ],
      ),
    );
  }
}
